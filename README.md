# HERE Coding Challenge 

This repo is owned by Stefano Bonetta and contains a possible solution to the proposed assignment.

Brief description
----
The project is structured following the **MVP (Model View Presenter)** pattern in order to boost the separation of concerns between Android's Actvities/Fragment, data and logic. 
Using MVP it's possible to move most of logic from activities/fragments to *presenters* so that they can be tested without using instrumentation tests. This means that, as general rule, the *presenter* acts as the mediator between *model* and *view*, which are ideally isolated from each others.

In order to consolidate this aspect, some **unit tests** (for *presenters*) have been added with the aim of demonstrating the meaning of the overall architecture.

The project is organized in the following packages:

 - **org.heretothere.common** 
 - **org.heretothere.search** 
 - **org.heretothere.routing**
 - **org.heretothere.utils**

In order to ease the comprehension of the MVP implementation, the following subpackages can be found in the main packages when required:

 - ***.model**
 - ***.view**
 - ***.presenter**
 
The project's structure allows to easily extend/add new use cases while maintaining a clean, readable and understandable codebase.

Third party libraries
----

 - **HERE-sdk**: used for map rendering
 - **RecyclerView**: used to render the search result list
 - **Gson**: used to crunch JSON data from APIs into POJOs
 - **Retrofit**: used to handle netkwork operations
 - **ButterKnife**: used for smart views binding
 - **Picasso**: used to load remote images
 - **Mockito**: used as mocking framework for unit tests

Additional note on HERE-sdk
----
 The HERE-sdk has been used for map rendering only: **the route between selected points is calculated through the provided Routing REST API** (*https://route.cit.api.here.com/routing*) and rendered on the map as a *MapObject* (more precisely as a *MapRoute* created from a *GeoPolyline*). 
 
 The *RouteManager* class  would have been a good alternative for route calculation but it would have undermined the using of the Routing API requested by the challenge, therefore it has not been used.
 
Contacts
----
Stefano Bonetta (bonnyfone@gmail.com)

