package org.heretothere.utils;

import android.view.MotionEvent;
import android.view.View;

/**
 * A touch listener which applies alpha effect on the view.
 */
public class AlphaTouchListener implements View.OnTouchListener {


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * alpha level when pressed
     */
    private float alphaPressed;

    /**
     * alpha level when idle
     */
    private float alphaNormal;


    /* ---------------------------------------------------
     * CONSTRUCTORS
     * ------------------------------------------------- */

    public AlphaTouchListener(float alphaPressed, float alphaNormal) {
        this.alphaPressed = alphaPressed;
        this.alphaNormal = alphaNormal;
    }


    /* ---------------------------------------------------
     * CLASS METHODS
     * ------------------------------------------------- */

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                view.setAlpha(alphaPressed);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_OUTSIDE:
            case MotionEvent.ACTION_CANCEL:
                view.setAlpha(alphaNormal);
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_MOVE:
                break;

        }
        return false;
    }
}
