package org.heretothere.utils;

import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.PositioningManager;

/**
 * Utility class for location/position info.
 */
public class PositionUtils {

    /**
     * Return the last valid position available.
     *
     * @return a {@link String} containing latitude and longitude separated by comma.
     */
    public static String getLastValidPosition() {
        if (PositioningManager.getInstance() != null &&
                PositioningManager.getInstance().getLastKnownPosition() != null) {

            GeoPosition lastKnownPosition = PositioningManager.getInstance().getLastKnownPosition();
            return lastKnownPosition.getCoordinate().getLatitude() + ","
                    + lastKnownPosition.getCoordinate().getLongitude();
        } else {
            //return a default position in case we have no position
            return "52.5317,13.385";
        }
    }
}
