package org.heretothere.utils;

import android.os.Build;
import android.text.Html;
import android.text.Spanned;

/**
 * Utility class for HTML-related operations.
 */
public class HtmlUtils {


    /* ---------------------------------------------------
     * STATIC METHODS
     * ------------------------------------------------- */

    /**
     * Convert formatted {@link String} into spanned HTML.
     *
     * @param source the source {@link String} which contains HTML tags
     * @return a {@link Spanned} version of the source
     */
    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }
}
