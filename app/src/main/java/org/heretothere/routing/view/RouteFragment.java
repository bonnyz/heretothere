package org.heretothere.routing.view;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPolyline;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapPolyline;
import com.here.android.mpa.mapping.MapView;

import org.heretothere.R;
import org.heretothere.routing.presenter.RoutePresenterInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Fragment which handles the 'routing' use case.
 */
public class RouteFragment extends Fragment {


    /* ---------------------------------------------------
     * STATIC ATTRIBUTES
     * ------------------------------------------------- */

    public static final String TAG = "RouteFragment";

    private final static int REQUEST_CODE_PERMISSIONS = 1;

    /**
     * Permissions that need to be explicitly requested from the user
     */
    private static final String[] REQUIRED_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * Start point TextView
     */
    @BindView(R.id.route_txt_start_location)
    TextView txtStart;

    /**
     * Destination point TextView
     */
    @BindView(R.id.route_txt_end_location)
    TextView txtEnd;

    /**
     * Calculate button
     */
    @BindView(R.id.route_btn_calculate)
    Button btnCalculateRoute;

    /**
     * Map container
     */
    @BindView(R.id.fragment_route_map_container)
    ViewGroup containerMap;

    /**
     * Progress indicator
     */
    @BindView(R.id.fragment_route_progress_indicator)
    ViewGroup progressIndicator;

    /**
     * Unbinder ButterKnife
     */
    Unbinder unbinder;

    /**
     * MapView reference
     */
    MapView mapView;

    /**
     * Map reference
     */
    Map map;

    /**
     * Route polyline
     */
    GeoPolyline geoMapRoute;

    /**
     * MapRoute reference (can be added to a {@link MapView})
     */
    MapPolyline mapRoute;

    /**
     * Starting point marker
     */
    MapMarker mapMarkerStart;

    /**
     * Destination point marker
     */
    MapMarker mapMarkerEnd;

    /**
     * Flag for added map objects
     */
    private boolean mapObjectsAdded;


    /* ---------------------------------------------------
     * LIFE CYCLE
     * ------------------------------------------------- */

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_route, container, false);
        //bind views
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        checkPermissions();
        bindListeners();
    }

    @Override
    public void onPause() {
        super.onPause();

        //pause PositionManager/MapEngine/Map
        try {
            PositioningManager.getInstance().stop();
        } catch (NullPointerException npe) {
        }
        MapEngine.getInstance().onPause();
        if (mapView != null) {
            mapView.onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        //resume PositionManager/MapEngine/Map
        try {
            PositioningManager.getInstance().start(PositioningManager.LocationMethod.GPS_NETWORK);
        } catch (NullPointerException npe) {
        }

        MapEngine.getInstance().onResume();
        if (mapView != null) {
            mapView.onResume();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mapView = null;
        map = null;
        mapMarkerEnd = null;
        mapMarkerStart = null;
        mapRoute = null;
        geoMapRoute = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSIONS:
                for (int index = 0; index < permissions.length; index++) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getActivity(), "Permission error, unable to instatiate the" +
                                " map.", Toast.LENGTH_LONG).show();
                        return;
                    }
                }
                // all permissions were granted, init the map
                initMap();
                break;
        }
    }


    /* ---------------------------------------------------
     * CLASS METHODS
     * ------------------------------------------------- */

    private void bindListeners() {
        View.OnClickListener commonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RoutePresenterInterface presenter = ((RouteActivity) getActivity())
                        .getRoutePresenter();
                switch (view.getId()) {
                    case R.id.route_txt_start_location:
                        presenter.onUserSearchPlace(true);
                        break;

                    case R.id.route_txt_end_location:
                        presenter.onUserSearchPlace(false);
                        break;

                    case R.id.route_btn_calculate:
                        presenter.onUserCalculateRoute();
                        break;
                }
            }
        };

        txtStart.setOnClickListener(commonClickListener);
        txtEnd.setOnClickListener(commonClickListener);
        btnCalculateRoute.setOnClickListener(commonClickListener);
    }

    /**
     * Set the visibility of the loading indicator
     *
     * @param loading
     */
    void setLoading(boolean loading) {
        if (progressIndicator != null) {
            progressIndicator.setVisibility(loading ? View.VISIBLE : View.GONE);
        }
    }

    /**
     * Set the calculate button on/off
     *
     * @param enabled
     */
    void setCalculateButtonEnabled(boolean enabled) {
        if (btnCalculateRoute != null) {
            btnCalculateRoute.setEnabled(enabled);
            btnCalculateRoute.setTextColor(ContextCompat.getColor(getContext(),
                    enabled ? R.color.colorNeutral : R.color.colorTextHint));
        }
    }

    /**
     * Set the map's route
     *
     * @param newRoute a {@link GeoPolyline} which defines the route, or null to
     *                 clear the existing route
     */
    void setRoute(@Nullable GeoPolyline newRoute) {
        geoMapRoute = newRoute;
        if (map != null) {

            //Clean map if needed
            if (mapObjectsAdded) {
                map.removeMapObject(mapRoute);
                map.removeMapObject(mapMarkerStart);
                map.removeMapObject(mapMarkerEnd);
            }

            //Update map with new route if possible
            if (newRoute != null && newRoute.getNumberOfPoints() > 0) {
                GeoCoordinate startPoint = newRoute.getPoint(0);
                GeoCoordinate endPoint = newRoute.getPoint(newRoute.getNumberOfPoints() - 1);
                Image imageMarkerStart = new Image();
                Image imageMarkerEnd = new Image();
                try {
                    imageMarkerStart.setImageResource(R.drawable.ic_location_start);
                    imageMarkerEnd.setImageResource(R.drawable.ic_location_end);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mapMarkerStart = new MapMarker(startPoint, imageMarkerStart);
                mapMarkerEnd = new MapMarker(endPoint, imageMarkerEnd);
                mapRoute = new MapPolyline(newRoute);
                mapRoute.setLineColor(ContextCompat.getColor(getContext(), R.color.colorRoute));
                mapRoute.setLineWidth(20);
                map.addMapObject(mapRoute);
                map.addMapObject(mapMarkerStart);
                map.addMapObject(mapMarkerEnd);
                map.zoomTo(newRoute.getBoundingBox(), Map.Animation.LINEAR, Map
                        .MOVE_PRESERVE_ORIENTATION);
                mapObjectsAdded = true;
                //map.getPositionIndicator().setVisible()
            }
        }
    }

    /**
     * Set the description of the starting/ending point
     *
     * @param startingPoint
     * @param description
     */
    void setPointDescription(boolean startingPoint, String description) {
        if (startingPoint && txtStart != null) {
            txtStart.setText(description);
        } else if (!startingPoint && txtEnd != null) {
            txtEnd.setText(description);
        }
    }

    /**
     * Internal method which init the map asynchronously
     */
    private void initMap() {
        MapEngine.getInstance().init(getContext(), new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    PositioningManager.getInstance().start(PositioningManager.LocationMethod
                            .GPS_NETWORK);
                    mapView = new MapView(getContext());
                    ViewGroup.LayoutParams lp =
                            new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.MATCH_PARENT);
                    containerMap.addView(mapView, 0, lp);
                    map = new Map();
                    mapView.setMap(map);
                    map.getPositionIndicator().setVisible(true);

                    //set the route if we already have it
                    if (geoMapRoute != null) {
                        setRoute(geoMapRoute);
                    }
                } else {
                    Toast.makeText(getActivity(), "Map error:" + error.getDetails(), Toast
                            .LENGTH_LONG).show();
                    Log.w(TAG, "Map error: " + error.getDetails());
                }
            }
        });
    }

    /**
     * Internal method to check the dynamically controlled permissions and requests missing
     * permissions from the user.
     */
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check required dynamic permissions
        for (final String permission : REQUIRED_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(getContext(), permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request missing permissions
            final String[] permissions = missingPermissions.toArray(new String[missingPermissions
                    .size()]);
            requestPermissions(permissions, REQUEST_CODE_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_PERMISSIONS, REQUIRED_PERMISSIONS,
                    grantResults);
        }
    }

}
