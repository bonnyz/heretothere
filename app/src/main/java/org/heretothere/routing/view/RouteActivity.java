package org.heretothere.routing.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.here.android.mpa.common.GeoPolyline;

import org.heretothere.R;
import org.heretothere.common.presenter.PresenterStateMaintainerFragment;
import org.heretothere.routing.presenter.RoutePresenter;
import org.heretothere.routing.presenter.RoutePresenterInterface;
import org.heretothere.search.view.SearchActivity;
import org.heretothere.search.view.SearchResultItem;


/**
 * (MVP) VIEW implementation of the 'Routing' use case.
 */
public class RouteActivity extends AppCompatActivity implements RouteViewInterface {


    /* ---------------------------------------------------
     * STATIC ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * Request code for starting point
     */
    public static final int REQUEST_CODE_SEARCH_STARTING_POINT = 4;

    /**
     * Request code for destination point
     */
    public static final int REQUEST_CODE_SEARCH_ENDING_POINT = 5;


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * Fragment which contains the 'Routing' ui
     */
    private RouteFragment routeFragment;

    /**
     * (MVP) PRESENTER reference for the 'Routing' use case
     */
    private RoutePresenterInterface routePresenter;


    /* ---------------------------------------------------
     * LIFE CYCLE
     * ------------------------------------------------- */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_route);
        FragmentManager fm = getSupportFragmentManager();

        //Obtain a PRESENTER instance
        String retainTag = RoutePresenter.RETAIN_TAG;
        Fragment presenterStateMaintainer = fm.findFragmentByTag(retainTag);
        if (presenterStateMaintainer == null) {
            //there's no cached PRESENTER, create a new one and preserve it in a retainInstance
            // ui-less fragment
            routePresenter = new RoutePresenter(this);
            FragmentTransaction transaction = fm.beginTransaction();
            PresenterStateMaintainerFragment maintainerFragment = new
                    PresenterStateMaintainerFragment();
            maintainerFragment.setPresenter(routePresenter);
            transaction.add(maintainerFragment, retainTag);
            transaction.commit();
        } else {
            //retrieve the PRESENTER from the state maintainer fragment
            routePresenter = (RoutePresenter) ((PresenterStateMaintainerFragment)
                    presenterStateMaintainer).getPresenter();
            //notify the PRESENTER that the VIEW is changed
            routePresenter.onViewChanged(this);
        }

        //Create/retrieve the RouteFragment
        Fragment cachedRouteFragment = fm.findFragmentByTag(RouteFragment.TAG);
        if (cachedRouteFragment != null && cachedRouteFragment instanceof RouteFragment) {
            routeFragment = (RouteFragment) cachedRouteFragment;
        } else {
            routeFragment = new RouteFragment();
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.activity_map_route_container, routeFragment, RouteFragment
                    .TAG);
            transaction.commit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //accept results from search
        if (SearchActivity.RESULT_CODE_SUCCESS == resultCode) {
            SearchResultItem result = data.getParcelableExtra(SearchActivity.RESULT_KEY);
            switch (requestCode) {
                case REQUEST_CODE_SEARCH_STARTING_POINT:
                    routePresenter.onUserPlaceSelected(true, result);
                    break;

                case REQUEST_CODE_SEARCH_ENDING_POINT:
                    routePresenter.onUserPlaceSelected(false, result);
                    break;
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //notify PRESENTER: view is ready
        routePresenter.onViewReady();
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //notify PRESENTER: view as been destroyed
        routePresenter.onViewDestroyed();
        routePresenter = null;
    }


    /* ---------------------------------------------------
     * MVP
     * ------------------------------------------------- */

    @Override
    public void showLoadingIndicator() {
        routeFragment.setLoading(true);
    }

    @Override
    public void hideLoadingIndicator() {
        routeFragment.setLoading(false);
    }

    @Override
    public void updateStartPoint(SearchResultItem startPoint) {
        if (startPoint != null) {
            routeFragment.setPointDescription(true, startPoint.getTitle());
        }
    }

    @Override
    public void updateEndPoint(SearchResultItem endPoint) {
        if (endPoint != null) {
            routeFragment.setPointDescription(false, endPoint.getTitle());
        }
    }

    @Override
    public void updateRoute(GeoPolyline routePolyLine) {
        routeFragment.setRoute(routePolyLine);
    }

    @Override
    public void openSearch(boolean startPoint) {
        Intent startSearch = new Intent(this, SearchActivity.class);
        startActivityForResult(startSearch, startPoint ?
                REQUEST_CODE_SEARCH_STARTING_POINT : REQUEST_CODE_SEARCH_ENDING_POINT);
    }

    @Override
    public void enableCalculateRoute() {
        routeFragment.setCalculateButtonEnabled(true);
    }

    @Override
    public void disableCalculateRoute() {
        routeFragment.setCalculateButtonEnabled(false);
    }

    @Override
    public void showNoRouteError() {
        Toast.makeText(this, R.string.no_route_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showSamePointsError() {
        Toast.makeText(this, R.string.no_route_same_points, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.generic_error, Toast.LENGTH_LONG).show();
    }

    /**
     * Return the 'Route' presenter
     *
     * @return a {@link RoutePresenterInterface}
     */
    public RoutePresenterInterface getRoutePresenter() {
        return routePresenter;
    }
}
