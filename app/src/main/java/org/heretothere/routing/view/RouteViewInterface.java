package org.heretothere.routing.view;

import com.here.android.mpa.common.GeoPolyline;

import org.heretothere.common.view.CommonViewInterface;
import org.heretothere.search.view.SearchResultItem;

/**
 * (MVP) VIEW Interface for the 'Routing' use case.
 */
public interface RouteViewInterface extends CommonViewInterface {

    /**
     * Update the starting point
     *
     * @param startPoint a {@link SearchResultItem} to use as starting point
     */
    void updateStartPoint(SearchResultItem startPoint);

    /**
     * Update the ending point
     *
     * @param endPoint a {@link SearchResultItem} to use as destination point
     */
    void updateEndPoint(SearchResultItem endPoint);

    /**
     * Update the route using the provided {@link GeoPolyline}
     *
     * @param routePolyLine a {@link GeoPolyline} representing the new route
     */
    void updateRoute(GeoPolyline routePolyLine);

    /**
     * Open the search function ('Search' use case) to select the starting/destination point
     *
     * @param startPoint true if we want to select the starting point, false if we want to select
     *                   the destination point
     */
    void openSearch(boolean startPoint);

    /**
     * Enable the 'calculate route' function
     */
    void enableCalculateRoute();

    /**
     * Disable the 'calculate route' function
     */
    void disableCalculateRoute();

    /**
     * Show a specific error when there's no available routes
     */
    void showNoRouteError();

    /**
     * Show a specific error when we are calculating a route between two equals points
     */
    void showSamePointsError();

}
