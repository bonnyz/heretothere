package org.heretothere.routing.presenter;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPolyline;

import org.heretothere.common.model.api.pojos.CalculatedRoute;
import org.heretothere.common.model.api.pojos.Leg;
import org.heretothere.common.model.api.pojos.Maneuver;
import org.heretothere.common.model.api.pojos.MappedPosition;
import org.heretothere.common.presenter.CommonPresenter;
import org.heretothere.routing.model.RouteModel;
import org.heretothere.routing.model.RouteModelInterface;
import org.heretothere.routing.model.RouteModelResponseCallback;
import org.heretothere.routing.view.RouteViewInterface;
import org.heretothere.search.view.SearchResultItem;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * (MVP) PRESENTER implementation of the 'Routing' use case.
 */
public class RoutePresenter extends CommonPresenter implements RoutePresenterInterface {


    /* ---------------------------------------------------
     * STATIC ATTRIBUTES
     * ------------------------------------------------- */

    public static final String TAG = "RoutePresenter";

    public static final String RETAIN_TAG = TAG + "_retain";

    /**
     * Default parameters for route mode
     */
    private static final String DEFAULT_ROUTE_MODE = "fastest;car;traffic:disabled";

    /**
     * Default parameters for route attributes
     */
    private static final String DEFAULT_ROUTE_ATTRIBUTES = "sh,wp";


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * Routing VIEW reference
     */
    private WeakReference<RouteViewInterface> weakRouteViewInterface;

    /**
     * Routing MODEL reference
     */
    private RouteModelInterface routeModel;

    /**
     * Starting point
     */
    private SearchResultItem routeStart;

    /**
     * Destination point
     */
    private SearchResultItem routeEnd;


    /* ---------------------------------------------------
     * CONSTRUCTORS
     * ------------------------------------------------- */

    public RoutePresenter(RouteViewInterface view) {
        weakRouteViewInterface = new WeakReference<RouteViewInterface>(view);
        routeModel = new RouteModel();
    }

    public RoutePresenter(RouteViewInterface view, RouteModelInterface model) {
        weakRouteViewInterface = new WeakReference<RouteViewInterface>(view);
        routeModel = model;
    }


    /* ---------------------------------------------------
     * MVP
     * ------------------------------------------------- */

    @Override
    public void onUserSearchPlace(boolean startPoint) {
        if (weakRouteViewInterface != null && weakRouteViewInterface.get() != null) {
            weakRouteViewInterface.get().openSearch(startPoint);
        }
    }

    @Override
    public void onUserPlaceSelected(boolean start, SearchResultItem selected) {
        if (selected != null) {
            if (start) {
                routeStart = selected;
            } else {
                routeEnd = selected;
            }
        }

        if (weakRouteViewInterface != null && weakRouteViewInterface.get() != null) {
            if (start) {
                weakRouteViewInterface.get().updateStartPoint(routeStart);
            } else {
                weakRouteViewInterface.get().updateEndPoint(routeEnd);
            }
            updateCalculateButtonState();
        }
    }

    @Override
    public void onUserCalculateRoute() {
        if (routeStart != null && routeEnd != null) {

            String routeStartString = prepareGeoPoint(routeStart);
            String routeEndString = prepareGeoPoint(routeEnd);

            //Check if we are using the same points
            if (routeStartString != null && routeStartString.equals(routeEndString)) {
                if (weakRouteViewInterface != null && weakRouteViewInterface.get() != null) {
                    weakRouteViewInterface.get().showSamePointsError();
                }
                return;
            }

            //Switch to loading state
            if (weakRouteViewInterface != null && weakRouteViewInterface.get() != null) {
                weakRouteViewInterface.get().showLoadingIndicator();
            }

            routeModel.getRoute(routeStartString, routeEndString,
                    DEFAULT_ROUTE_MODE, DEFAULT_ROUTE_ATTRIBUTES, new RouteModelResponseCallback() {
                        @Override
                        public void onResult(CalculatedRoute result) {
                            if (weakRouteViewInterface != null &&
                                    weakRouteViewInterface.get() != null) {

                                weakRouteViewInterface.get().hideLoadingIndicator();
                                if (result == null) {
                                    weakRouteViewInterface.get().showNoRouteError();
                                } else {
                                    weakRouteViewInterface.get().updateRoute(prepareRoutePolyline
                                            (result));
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable error) {
                            if (weakRouteViewInterface != null &&
                                    weakRouteViewInterface.get() != null) {

                                weakRouteViewInterface.get().hideLoadingIndicator();
                                weakRouteViewInterface.get().showError();
                            }
                        }
                    });

        }
    }

    @Override
    public void onViewChanged(RouteViewInterface view) {
        weakRouteViewInterface = new WeakReference<RouteViewInterface>(view);
        restoreOnViewReady = true;
    }

    @Override
    public void onViewReady() {
        if (shouldRestoreDataOnViewReady() && weakRouteViewInterface != null &&
                weakRouteViewInterface.get()
                != null) {
            restoreOnViewReady = false;
            RouteViewInterface v = weakRouteViewInterface.get();

            //Restore model state
            if (routeModel.isLoading()) {
                v.showLoadingIndicator();
            } else if (routeModel.hasResultData()) {
                v.updateRoute(prepareRoutePolyline(routeModel.getLastCalculatedRoute()));
            }

            //Restore inputs state
            if (routeStart != null) {
                v.updateStartPoint(routeStart);
            }
            if (routeEnd != null) {
                v.updateEndPoint(routeEnd);
            }
            updateCalculateButtonState();
        }
    }

    @Override
    public void onViewDestroyed() {
        weakRouteViewInterface = null;
    }

    /**
     * Internal method to update the 'calculate route' function state
     */
    private void updateCalculateButtonState() {
        if (weakRouteViewInterface != null && weakRouteViewInterface.get() != null) {
            if (routeStart != null && routeEnd != null) {
                weakRouteViewInterface.get().enableCalculateRoute();
            } else {
                weakRouteViewInterface.get().disableCalculateRoute();
            }
        }
    }

    /**
     * Create a readable string containing the geo-position of the provided {@link SearchResultItem}
     *
     * @param point a {@link SearchResultItem}
     * @return a {@link String} containing the geo-position
     */
    public String prepareGeoPoint(SearchResultItem point) { //TODO test
        return "geo!" + point.getGeoPosition().get(0) + "," + point.getGeoPosition().get(1);
    }

    /**
     * Creates a {@link GeoPolyline} from calculated route
     *
     * @param calculatedRoute a raw {@link CalculatedRoute}
     * @return a {@link GeoPolyline} containing the route
     */
    private GeoPolyline prepareRoutePolyline(CalculatedRoute calculatedRoute) { //TODO test
        GeoPolyline polyline = new GeoPolyline();
        if (calculatedRoute != null && calculatedRoute.getResponse() != null
                && calculatedRoute.getResponse().getRoute() != null
                && calculatedRoute.getResponse().getRoute().size() > 0) {

            //TODO: give the user the ability to select which route to display
            //For now, we choose the FIRST route!
            List<Leg> legs = calculatedRoute.getResponse().getRoute().get(0).getLeg();
            for (Leg leg : legs) {
                MappedPosition legStartMappedPosition = leg.getStart().getMappedPosition();
                MappedPosition legEndMappedPosition = leg.getEnd().getMappedPosition();

                polyline.add(new GeoCoordinate(legStartMappedPosition.getLatitude(),
                        legStartMappedPosition.getLongitude()));
                if (leg.getManeuver() != null) {
                    for (Maneuver maneuver : leg.getManeuver()) {
                        polyline.add(new GeoCoordinate(maneuver.getPosition().getLatitude(),
                                maneuver.getPosition().getLongitude()));
                    }
                }
                polyline.add(new GeoCoordinate(legEndMappedPosition.getLatitude(),
                        legEndMappedPosition.getLongitude()));
            }
        }
        return polyline;
    }

    /**
     * Return the reference to the {@link RouteViewInterface}
     *
     * @return a {@link RouteViewInterface}
     */
    public RouteViewInterface getRouteViewInterface() {
        if (weakRouteViewInterface != null) {
            return weakRouteViewInterface.get();
        }
        return null;
    }

}
