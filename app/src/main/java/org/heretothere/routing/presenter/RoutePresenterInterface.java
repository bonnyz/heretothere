package org.heretothere.routing.presenter;


import org.heretothere.common.presenter.CommonPresenterInterface;
import org.heretothere.routing.view.RouteViewInterface;
import org.heretothere.search.view.SearchResultItem;

/**
 * (MVP) PRESENTER Interface for the 'Routing' use case.
 */
public interface RoutePresenterInterface extends CommonPresenterInterface {

    /**
     * Called when the user want to search for a place
     *
     * @param start true if the user wants to search for the starting place, false if the user
     *              wants to search for the destination place
     */
    void onUserSearchPlace(boolean start);

    /**
     * Called when the user select a place
     *
     * @param start    true if the user selected the starting place, false if the user selected the
     *                 destination place
     * @param selected the {@link SearchResultItem} which contains the selected place
     */
    void onUserPlaceSelected(boolean start, SearchResultItem selected);

    /**
     * Called when the user wants to calculate the route
     */
    void onUserCalculateRoute();

    /**
     * Called when the VIEW is changed (e.g. on configuration changes)
     *
     * @param view the new RouteViewInterface
     */
    void onViewChanged(RouteViewInterface view);
}
