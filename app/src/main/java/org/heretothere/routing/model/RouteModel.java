package org.heretothere.routing.model;

import org.heretothere.common.model.CommonModel;
import org.heretothere.common.model.api.RoutingRestClient;
import org.heretothere.common.model.api.pojos.CalculatedRoute;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * (MVP) MODEL implementation of the 'Routing' use case.
 */
public class RouteModel extends CommonModel implements RouteModelInterface {


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * The last calculated route
     */
    private CalculatedRoute lastCalculatedRoute;


    /* ---------------------------------------------------
     * MVP
     * ------------------------------------------------- */

    @Override
    public void getRoute(String fromGeoPoint, String toGeoPoint, String routeMode, String
            routeAttributes, final RouteModelResponseCallback responseCallback) {

        //Retrieve route
        isLoading = true;
        lastCalculatedRoute = null;

        Call<CalculatedRoute> routeRequest = RoutingRestClient.getInstance().getRoutingInterface()
                .calculateRoute(fromGeoPoint, toGeoPoint, routeMode, routeAttributes);

        routeRequest.enqueue(new Callback<CalculatedRoute>() {
            @Override
            public void onResponse(Call<CalculatedRoute> call, Response<CalculatedRoute> response) {
                lastCalculatedRoute = response.body();
                hasErrors = false;
                isLoading = false;
                responseCallback.onResult(lastCalculatedRoute); //notify result
            }

            @Override
            public void onFailure(Call<CalculatedRoute> call, Throwable t) {
                lastCalculatedRoute = null;
                hasErrors = true;
                isLoading = false;
                responseCallback.onError(t); //notify error
            }
        });
    }

    @Override
    public CalculatedRoute getLastCalculatedRoute() {
        return lastCalculatedRoute;
    }

    @Override
    public boolean hasResultData() {
        return lastCalculatedRoute != null;
    }

}
