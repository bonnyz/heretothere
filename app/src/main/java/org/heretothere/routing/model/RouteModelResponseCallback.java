package org.heretothere.routing.model;

import org.heretothere.common.model.FallibleResponseCallback;
import org.heretothere.common.model.api.pojos.CalculatedRoute;

/**
 * Callback used by instances of {@link RouteModelInterface} to communicate results back.<br>
 * See {@link RouteModelInterface} for more info.
 */
public interface RouteModelResponseCallback extends FallibleResponseCallback {

    /**
     * Called when a valid route is received.
     *
     * @param result a {@link CalculatedRoute}
     */
    void onResult(CalculatedRoute result);

}
