package org.heretothere.routing.model;


import org.heretothere.common.model.CommonModelInterface;
import org.heretothere.common.model.api.pojos.CalculatedRoute;

/**
 * (MVP) MODEL Interface for the 'Routing' use case.
 */
public interface RouteModelInterface extends CommonModelInterface {

    /**
     * Get a route from a starting point to a destination point
     *
     * @param fromGeoPoint     a geo-position to use as starting point
     * @param toGeoPoint       a geo-position to use as destination point
     * @param routeMode        route mode
     * @param routeAttributes  route attributes
     * @param responseCallback a {@link RouteModelResponseCallback} to receive the result
     *                         asynchronously
     */
    void getRoute(String fromGeoPoint, String toGeoPoint, String routeMode,
                  String routeAttributes, RouteModelResponseCallback responseCallback);

    /**
     * Get the last calculated route
     *
     * @return a {@link CalculatedRoute}, or null if there's no calculated route
     */
    CalculatedRoute getLastCalculatedRoute();

}
