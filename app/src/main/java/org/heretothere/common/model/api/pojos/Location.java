package org.heretothere.common.model.api.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Location {

    @SerializedName("position")
    @Expose
    private List<Double> position = new ArrayList<Double>();
    @SerializedName("address")
    @Expose
    private Address address;

    /**
     * No args constructor for use in serialization
     */
    public Location() {
    }

    /**
     * @param position
     * @param address
     */
    public Location(List<Double> position, Address address) {
        this.position = position;
        this.address = address;
    }

    /**
     * @return The position
     */
    public List<Double> getPosition() {
        return position;
    }

    /**
     * @param position The position
     */
    public void setPosition(List<Double> position) {
        this.position = position;
    }

    /**
     * @return The address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

}
