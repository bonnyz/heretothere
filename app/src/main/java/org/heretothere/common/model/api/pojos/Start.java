package org.heretothere.common.model.api.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Start {

    @SerializedName("linkId")
    @Expose
    private String linkId;
    @SerializedName("mappedPosition")
    @Expose
    private MappedPosition mappedPosition;
    @SerializedName("originalPosition")
    @Expose
    private OriginalPosition originalPosition;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("spot")
    @Expose
    private Double spot;
    @SerializedName("sideOfStreet")
    @Expose
    private String sideOfStreet;
    @SerializedName("mappedRoadName")
    @Expose
    private String mappedRoadName;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("shapeIndex")
    @Expose
    private Integer shapeIndex;

    /**
     * No args constructor for use in serialization
     */
    public Start() {
    }

    /**
     * @param originalPosition
     * @param linkId
     * @param mappedPosition
     * @param sideOfStreet
     * @param shapeIndex
     * @param mappedRoadName
     * @param label
     * @param type
     * @param spot
     */
    public Start(String linkId, MappedPosition mappedPosition, OriginalPosition originalPosition,
                 String type, Double spot, String sideOfStreet, String mappedRoadName, String
                         label, Integer shapeIndex) {
        this.linkId = linkId;
        this.mappedPosition = mappedPosition;
        this.originalPosition = originalPosition;
        this.type = type;
        this.spot = spot;
        this.sideOfStreet = sideOfStreet;
        this.mappedRoadName = mappedRoadName;
        this.label = label;
        this.shapeIndex = shapeIndex;
    }

    /**
     * @return The linkId
     */
    public String getLinkId() {
        return linkId;
    }

    /**
     * @param linkId The linkId
     */
    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    /**
     * @return The mappedPosition
     */
    public MappedPosition getMappedPosition() {
        return mappedPosition;
    }

    /**
     * @param mappedPosition The mappedPosition
     */
    public void setMappedPosition(MappedPosition mappedPosition) {
        this.mappedPosition = mappedPosition;
    }

    /**
     * @return The originalPosition
     */
    public OriginalPosition getOriginalPosition() {
        return originalPosition;
    }

    /**
     * @param originalPosition The originalPosition
     */
    public void setOriginalPosition(OriginalPosition originalPosition) {
        this.originalPosition = originalPosition;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The spot
     */
    public Double getSpot() {
        return spot;
    }

    /**
     * @param spot The spot
     */
    public void setSpot(Double spot) {
        this.spot = spot;
    }

    /**
     * @return The sideOfStreet
     */
    public String getSideOfStreet() {
        return sideOfStreet;
    }

    /**
     * @param sideOfStreet The sideOfStreet
     */
    public void setSideOfStreet(String sideOfStreet) {
        this.sideOfStreet = sideOfStreet;
    }

    /**
     * @return The mappedRoadName
     */
    public String getMappedRoadName() {
        return mappedRoadName;
    }

    /**
     * @param mappedRoadName The mappedRoadName
     */
    public void setMappedRoadName(String mappedRoadName) {
        this.mappedRoadName = mappedRoadName;
    }

    /**
     * @return The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return The shapeIndex
     */
    public Integer getShapeIndex() {
        return shapeIndex;
    }

    /**
     * @param shapeIndex The shapeIndex
     */
    public void setShapeIndex(Integer shapeIndex) {
        this.shapeIndex = shapeIndex;
    }

}
