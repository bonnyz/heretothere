package org.heretothere.common.model.api;

import org.heretothere.common.model.api.pojos.SearchResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface used to properly instantiate the Retrofit client
 * used for the communication. It defines the set of operations
 * available with the Routing API.
 */
public interface PlacesApiInterface {

    /**
     * Search places around a location
     *
     * @param location a geo-position
     * @param query    the query to search
     * @return a {@link SearchResult} with the places that match your query
     */
    @GET("places/v1/discover/search")
    Call<SearchResult> searchPlaces(@Query("at") String location, @Query("q") String query);

}
