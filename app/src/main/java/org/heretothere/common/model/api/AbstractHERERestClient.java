package org.heretothere.common.model.api;


import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * An abstract REST client for the HERE APIs
 */
abstract class AbstractHERERestClient {


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * HERE API app-id
     */
    private final String APP_ID = "MB67wDl1eY3qMIdKCKxL";

    /**
     * HERE API app-code
     */
    private final String APP_CODE = "nRyBXaD_VXxviiqnOpJXuA";

    /**
     * REST parameter app-id
     */
    private final String PARAM_APP_ID = "app_id";

    /**
     * REST parameter app-code
     */
    private final String PARAM_APP_CODE = "app_code";

    /**
     * Retrofit instance used to handle network communications
     */
    private Retrofit retrofit;

    /**
     * API interface
     */
    private Object apiInterface;


    /* ---------------------------------------------------
     * CONSTRUCTORS
     * ------------------------------------------------- */

    AbstractHERERestClient() {
        init();
    }


    /* ---------------------------------------------------
     * CLASS METHODS
     * ------------------------------------------------- */

    /**
     * Get the base url used for API calls
     *
     * @return a {@link String} with the url
     */
    abstract String getBaseUrl();

    /**
     * Get the type of the API interface
     *
     * @return the {@link Class} type of the API interface
     */
    abstract Class getApiInterfaceType();

    /**
     * Init the REST client
     */
    private void init() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        //inject app-id and app-code
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl url = original.url().newBuilder()
                        .addQueryParameter(PARAM_APP_ID, APP_ID)
                        .addQueryParameter(PARAM_APP_CODE, APP_CODE)
                        .build();

                Request request = original.newBuilder().url(url).build();
                return chain.proceed(request);
            }
        });

        retrofit = new Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        apiInterface = retrofit.create(getApiInterfaceType());
    }

    /**
     * Return the raw API interface object. <br>
     * See {@link AbstractHERERestClient#getApiInterfaceType()}
     * for more information about the API interface type.
     *
     * @return the raw API interface object
     */
    protected Object getApiInterface() {
        return apiInterface;
    }

}
