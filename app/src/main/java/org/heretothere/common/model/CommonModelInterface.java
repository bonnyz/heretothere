package org.heretothere.common.model;

/**
 * (MVP) MODEL Interface for common use cases.
 */
public interface CommonModelInterface {

    /**
     * Check if the MODEL is loading data or not.
     *
     * @return true if the MODEL is loading data, false otherwise
     */
    boolean isLoading();

    /**
     * Check if the MODEL had errors while loading data.
     *
     * @return true if the MODEL had errors, false otherwise
     */
    boolean hasErrors();

    /**
     * Check if the MODEL already has cached data
     *
     * @return true if the MODEL has cached data, false otherwise
     */
    boolean hasResultData();
}
