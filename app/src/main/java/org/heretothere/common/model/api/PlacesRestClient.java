package org.heretothere.common.model.api;

/**
 * REST client for the HERE Places API.
 */
public class PlacesRestClient extends AbstractHERERestClient {


    /* ---------------------------------------------------
     * STATIC ATTRIBUTES
     * ------------------------------------------------- */

    private static final String PLACES_API_BASE_URL = "https://places.cit.api.here.com/";


    /* ---------------------------------------------------
     * SINGLETON
     * ------------------------------------------------- */

    private static PlacesRestClient singletonPlacesRestClient;

    public static PlacesRestClient getInstance() {
        if (singletonPlacesRestClient == null)
            singletonPlacesRestClient = new PlacesRestClient();

        return singletonPlacesRestClient;
    }

    private PlacesRestClient() {
        super();
    }


    /* ---------------------------------------------------
     * CLASS METHODS
     * ------------------------------------------------- */

    @Override
    String getBaseUrl() {
        return PLACES_API_BASE_URL;
    }

    @Override
    Class getApiInterfaceType() {
        return PlacesApiInterface.class;
    }

    /**
     * Get the remote interface to the Places API
     *
     * @return the {@link PlacesApiInterface}
     */
    public PlacesApiInterface getPlacesInterface() {
        return (PlacesApiInterface) getApiInterface();
    }

}
