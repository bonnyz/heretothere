package org.heretothere.common.model.api.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchResult {

    @SerializedName("results")
    @Expose
    private Results results;
    @SerializedName("search")
    @Expose
    private Search search;

    /**
     * No args constructor for use in serialization
     */
    public SearchResult() {
    }

    /**
     * @param results
     * @param search
     */
    public SearchResult(Results results, Search search) {
        this.results = results;
        this.search = search;
    }

    /**
     * @return The results
     */
    public Results getResults() {
        return results;
    }

    /**
     * @param results The results
     */
    public void setResults(Results results) {
        this.results = results;
    }

    /**
     * @return The search
     */
    public Search getSearch() {
        return search;
    }

    /**
     * @param search The search
     */
    public void setSearch(Search search) {
        this.search = search;
    }

}
