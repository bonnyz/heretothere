package org.heretothere.common.model;

/**
 * (MVP) MODEL for common use cases. Partially implements the {@link CommonModelInterface}
 */
public abstract class CommonModel implements CommonModelInterface {

    /**
     * Internal <i>loading</i> state flag
     */
    protected boolean isLoading;

    /**
     * Internal <i>error</i> state flag
     */
    protected boolean hasErrors;


    @Override
    public boolean hasErrors() {
        return hasErrors;
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }
}
