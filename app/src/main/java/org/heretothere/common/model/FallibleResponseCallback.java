package org.heretothere.common.model;

/**
 * A callback that can fail, throwing an error.
 */
public interface FallibleResponseCallback {

    /**
     * Called when an error occurred while retrieving results.
     *
     * @param error
     */
    void onError(Throwable error);
}
