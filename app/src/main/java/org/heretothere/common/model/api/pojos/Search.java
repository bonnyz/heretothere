package org.heretothere.common.model.api.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Search {

    @SerializedName("context")
    @Expose
    private Context context;

    /**
     * No args constructor for use in serialization
     */
    public Search() {
    }

    /**
     * @param context
     */
    public Search(Context context) {
        this.context = context;
    }

    /**
     * @return The context
     */
    public Context getContext() {
        return context;
    }

    /**
     * @param context The context
     */
    public void setContext(Context context) {
        this.context = context;
    }

}
