package org.heretothere.common.model.api.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Response {

    @SerializedName("metaInfo")
    @Expose
    private MetaInfo metaInfo;
    @SerializedName("route")
    @Expose
    private List<Route> route = new ArrayList<Route>();
    @SerializedName("language")
    @Expose
    private String language;

    /**
     * No args constructor for use in serialization
     */
    public Response() {
    }

    /**
     * @param route
     * @param metaInfo
     * @param language
     */
    public Response(MetaInfo metaInfo, List<Route> route, String language) {
        this.metaInfo = metaInfo;
        this.route = route;
        this.language = language;
    }

    /**
     * @return The metaInfo
     */
    public MetaInfo getMetaInfo() {
        return metaInfo;
    }

    /**
     * @param metaInfo The metaInfo
     */
    public void setMetaInfo(MetaInfo metaInfo) {
        this.metaInfo = metaInfo;
    }

    /**
     * @return The route
     */
    public List<Route> getRoute() {
        return route;
    }

    /**
     * @param route The route
     */
    public void setRoute(List<Route> route) {
        this.route = route;
    }

    /**
     * @return The language
     */
    public String getLanguage() {
        return language;
    }

    /**
     * @param language The language
     */
    public void setLanguage(String language) {
        this.language = language;
    }

}
