package org.heretothere.common.model.api.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Leg {

    @SerializedName("start")
    @Expose
    private Start start;
    @SerializedName("end")
    @Expose
    private End end;
    @SerializedName("length")
    @Expose
    private Integer length;
    @SerializedName("travelTime")
    @Expose
    private Integer travelTime;
    @SerializedName("maneuver")
    @Expose
    private List<Maneuver> maneuver = new ArrayList<Maneuver>();

    /**
     * No args constructor for use in serialization
     */
    public Leg() {
    }

    /**
     * @param travelTime
     * @param start
     * @param length
     * @param maneuver
     * @param end
     */
    public Leg(Start start, End end, Integer length, Integer travelTime, List<Maneuver> maneuver) {
        this.start = start;
        this.end = end;
        this.length = length;
        this.travelTime = travelTime;
        this.maneuver = maneuver;
    }

    /**
     * @return The start
     */
    public Start getStart() {
        return start;
    }

    /**
     * @param start The start
     */
    public void setStart(Start start) {
        this.start = start;
    }

    /**
     * @return The end
     */
    public End getEnd() {
        return end;
    }

    /**
     * @param end The end
     */
    public void setEnd(End end) {
        this.end = end;
    }

    /**
     * @return The length
     */
    public Integer getLength() {
        return length;
    }

    /**
     * @param length The length
     */
    public void setLength(Integer length) {
        this.length = length;
    }

    /**
     * @return The travelTime
     */
    public Integer getTravelTime() {
        return travelTime;
    }

    /**
     * @param travelTime The travelTime
     */
    public void setTravelTime(Integer travelTime) {
        this.travelTime = travelTime;
    }

    /**
     * @return The maneuver
     */
    public List<Maneuver> getManeuver() {
        return maneuver;
    }

    /**
     * @param maneuver The maneuver
     */
    public void setManeuver(List<Maneuver> maneuver) {
        this.maneuver = maneuver;
    }

}
