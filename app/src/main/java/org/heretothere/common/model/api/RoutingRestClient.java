package org.heretothere.common.model.api;

/**
 * REST client for the HERE Routing API.
 */
public class RoutingRestClient extends AbstractHERERestClient {


    /* ---------------------------------------------------
     * STATIC ATTRIBUTES
     * ------------------------------------------------- */

    private static final String ROUTING_API_BASE_URL = "https://route.cit.api.here.com/";


    /* ---------------------------------------------------
     * SINGLETON
     * ------------------------------------------------- */

    private static RoutingRestClient singletonRountingRestClient;

    public static RoutingRestClient getInstance() {
        if (singletonRountingRestClient == null)
            singletonRountingRestClient = new RoutingRestClient();

        return singletonRountingRestClient;
    }

    private RoutingRestClient() {
        super();
    }


    /* ---------------------------------------------------
     * CLASS METHODS
     * ------------------------------------------------- */

    @Override
    String getBaseUrl() {
        return ROUTING_API_BASE_URL;
    }

    @Override
    Class getApiInterfaceType() {
        return RoutingApiInterface.class;
    }

    /**
     * Get the remote interface to the Routing API
     *
     * @return the {@link RoutingApiInterface}
     */
    public RoutingApiInterface getRoutingInterface() {
        return (RoutingApiInterface) getApiInterface();
    }
}
