package org.heretothere.common.model.api.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Item {

    @SerializedName("position")
    @Expose
    private List<Double> position = new ArrayList<Double>();
    @SerializedName("bbox")
    @Expose
    private List<Double> bbox = new ArrayList<Double>();
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("category")
    @Expose
    private Category category;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("vicinity")
    @Expose
    private String vicinity;
    @SerializedName("having")
    @Expose
    private List<Object> having = new ArrayList<Object>();
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("href")
    @Expose
    private String href;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("authoritative")
    @Expose
    private Boolean authoritative;

    /**
     * No args constructor for use in serialization
     */
    public Item() {
    }

    /**
     * @param id
     * @param position
     * @param icon
     * @param category
     * @param title
     * @param distance
     * @param vicinity
     * @param bbox
     * @param having
     * @param authoritative
     * @param type
     * @param href
     */
    public Item(List<Double> position, List<Double> bbox, Integer distance, String title,
                Category category, String icon, String vicinity, List<Object> having, String
                        type, String href, String id, Boolean authoritative) {
        this.position = position;
        this.bbox = bbox;
        this.distance = distance;
        this.title = title;
        this.category = category;
        this.icon = icon;
        this.vicinity = vicinity;
        this.having = having;
        this.type = type;
        this.href = href;
        this.id = id;
        this.authoritative = authoritative;
    }

    /**
     * @return The position
     */
    public List<Double> getPosition() {
        return position;
    }

    /**
     * @param position The position
     */
    public void setPosition(List<Double> position) {
        this.position = position;
    }

    /**
     * @return The bbox
     */
    public List<Double> getBbox() {
        return bbox;
    }

    /**
     * @param bbox The bbox
     */
    public void setBbox(List<Double> bbox) {
        this.bbox = bbox;
    }

    /**
     * @return The distance
     */
    public Integer getDistance() {
        return distance;
    }

    /**
     * @param distance The distance
     */
    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * @param category The category
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * @return The icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * @param icon The icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * @return The vicinity
     */
    public String getVicinity() {
        return vicinity;
    }

    /**
     * @param vicinity The vicinity
     */
    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    /**
     * @return The having
     */
    public List<Object> getHaving() {
        return having;
    }

    /**
     * @param having The having
     */
    public void setHaving(List<Object> having) {
        this.having = having;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The href
     */
    public String getHref() {
        return href;
    }

    /**
     * @param href The href
     */
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The authoritative
     */
    public Boolean getAuthoritative() {
        return authoritative;
    }

    /**
     * @param authoritative The authoritative
     */
    public void setAuthoritative(Boolean authoritative) {
        this.authoritative = authoritative;
    }

}
