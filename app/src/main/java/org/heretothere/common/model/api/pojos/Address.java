package org.heretothere.common.model.api.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("house")
    @Expose
    private String house;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("postalCode")
    @Expose
    private String postalCode;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("county")
    @Expose
    private String county;
    @SerializedName("stateCode")
    @Expose
    private String stateCode;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;

    /**
     * No args constructor for use in serialization
     */
    public Address() {
    }

    /**
     * @param text
     * @param postalCode
     * @param county
     * @param street
     * @param countryCode
     * @param stateCode
     * @param house
     * @param district
     * @param country
     * @param city
     */
    public Address(String text, String house, String street, String postalCode, String district,
                   String city, String county, String stateCode, String country, String
                           countryCode) {
        this.text = text;
        this.house = house;
        this.street = street;
        this.postalCode = postalCode;
        this.district = district;
        this.city = city;
        this.county = county;
        this.stateCode = stateCode;
        this.country = country;
        this.countryCode = countryCode;
    }

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The house
     */
    public String getHouse() {
        return house;
    }

    /**
     * @param house The house
     */
    public void setHouse(String house) {
        this.house = house;
    }

    /**
     * @return The street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return The postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode The postalCode
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @return The district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * @param district The district
     */
    public void setDistrict(String district) {
        this.district = district;
    }

    /**
     * @return The city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return The county
     */
    public String getCounty() {
        return county;
    }

    /**
     * @param county The county
     */
    public void setCounty(String county) {
        this.county = county;
    }

    /**
     * @return The stateCode
     */
    public String getStateCode() {
        return stateCode;
    }

    /**
     * @param stateCode The stateCode
     */
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    /**
     * @return The country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return The countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode The countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

}
