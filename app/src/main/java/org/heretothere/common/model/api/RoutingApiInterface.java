package org.heretothere.common.model.api;

import org.heretothere.common.model.api.pojos.CalculatedRoute;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface used to properly instantiate the Retrofit client
 * used for the communication. It defines the set of operations
 * available with the Routing API.
 */
public interface RoutingApiInterface {

    /**
     * Get a route from two points
     *
     * @param fromGeoPoint    a geo-point to use as starting point
     * @param toGeoPoint      a geo-point to use as destination
     * @param mode            route mode
     * @param routeAttributes route attributes
     * @return a {@link CalculatedRoute} containing the routes found
     */
    @GET("routing/7.2/calculateroute.json")
    Call<CalculatedRoute> calculateRoute(@Query("waypoint0") String fromGeoPoint,
                                         @Query("waypoint1") String toGeoPoint,
                                         @Query("mode") String mode,
                                         @Query("routeAttributes") String routeAttributes);

}
