package org.heretothere.common.presenter;


/**
 * (MVP) PRESENTER for common use cases. Partially implements the {@link CommonPresenterInterface}
 */
public abstract class CommonPresenter implements CommonPresenterInterface {

    /**
     * Internal flag for restoring results when the view is ready
     */
    protected boolean restoreOnViewReady;

    @Override
    public boolean shouldRestoreDataOnViewReady() {
        return restoreOnViewReady;
    }
}
