package org.heretothere.common.presenter;

/**
 * (MVP) PRESENTER Interface for common use cases.
 */
public interface CommonPresenterInterface extends RetainablePresenter {

    /**
     * Called when the VIEW is ready
     */
    void onViewReady();

    /**
     * Called when the VIEW has been destroyed
     */
    void onViewDestroyed();

    /**
     * Check if the presenter should restore the data when the view is ready
     *
     * @return true it he data should be restored
     */
    boolean shouldRestoreDataOnViewReady();
}
