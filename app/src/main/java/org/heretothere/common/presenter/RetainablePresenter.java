package org.heretothere.common.presenter;


/**
 * A RetainablePresenter that can be retained across configuration changes
 * using a {@link PresenterStateMaintainerFragment}
 */
interface RetainablePresenter {

}
