package org.heretothere.common.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

/**
 * UI-less fragment used to maintain the state of a {@link RetainablePresenter}.<br>
 * This is a <b>setRetainInstance(true)</b> fragment.
 */
public class PresenterStateMaintainerFragment extends Fragment {


    /* ---------------------------------------------------
     * STATIC ATTRIBUTES
     * ------------------------------------------------- */

    public static final String TAG = "PresenterStateMaintainerFragment";


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * Internal reference to the {@link RetainablePresenter} to keep alive
     */
    private RetainablePresenter presenter;


    /* ---------------------------------------------------
     * LIFE CYCLE
     * ------------------------------------------------- */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //this is critical, we need to retain fragment's instance across configuration changes
        setRetainInstance(true);
    }

    /**
     * Set the {@link RetainablePresenter} to maintain
     *
     * @param presenter an instance of {@link RetainablePresenter}
     */
    public void setPresenter(RetainablePresenter presenter) {
        this.presenter = presenter;
    }

    /**
     * Get the maintained {@link RetainablePresenter}
     *
     * @return an instance of {@link RetainablePresenter}
     */
    public RetainablePresenter getPresenter() {
        return presenter;
    }

}
