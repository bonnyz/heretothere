package org.heretothere.common.view;

/**
 * (MVP) VIEW Interface for a generic use case.
 */
public interface CommonViewInterface {

    /**
     * Show the loading indicator.
     */
    void showLoadingIndicator();

    /**
     * Hide the loading indicator.
     */
    void hideLoadingIndicator();

    /**
     * Show an error message.
     */
    void showError();

}
