package org.heretothere.search.model;

import org.heretothere.common.model.FallibleResponseCallback;
import org.heretothere.common.model.api.pojos.SearchResult;


/**
 * Callback used by instances of {@link SearchModelInterface} to communicate results back.<br>
 * See {@link SearchModelInterface} for more info.
 */
public interface SearchModelResponseCallback extends FallibleResponseCallback {

    /**
     * Called when a valid search result is received.
     *
     * @param result a {@link SearchResult}
     */
    void onResult(SearchResult result);

}
