package org.heretothere.search.model;

import org.heretothere.common.model.CommonModelInterface;
import org.heretothere.common.model.api.pojos.SearchResult;

/**
 * (MVP) MODEL Interface for the 'Search' use case.
 */
public interface SearchModelInterface extends CommonModelInterface {

    /**
     * Get places based on the specified query and location
     *
     * @param location         a geo-position
     * @param query            the query to search
     * @param responseCallback a {@link SearchModelResponseCallback} to receive the result
     *                         asynchronously
     */
    void getPlacesByQuery(String location, String query, SearchModelResponseCallback
            responseCallback);

    /**
     * Get the last search result
     *
     * @return the last {@link SearchResult} , or null if there's no previous search result
     */
    SearchResult getLastSearchResult();

    /**
     * Get the last search query
     *
     * @return the last query {@link String}, or null if there's no previous search query
     */
    String getLastSearchQuery();

    /**
     * Get the query currently used for searching (the search could still be in process)
     *
     * @return the current query {@link String}, or null if there's no current search query
     */
    String getCurrentSearchingQuery();

}
