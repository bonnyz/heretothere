package org.heretothere.search.model;

import org.heretothere.common.model.CommonModel;
import org.heretothere.common.model.api.PlacesRestClient;
import org.heretothere.common.model.api.pojos.SearchResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * (MVP) MODEL implementation of the 'Search' use case.
 */
public class SearchModel extends CommonModel implements SearchModelInterface {


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * The last search result
     */
    private SearchResult lastSearchResult;

    /**
     * The last search query
     */
    private String lastSearchQuery;

    /**
     * The current search query
     */
    private String currentSearchQuery;


    /* ---------------------------------------------------
     * MVP
     * ------------------------------------------------- */

    @Override
    public void getPlacesByQuery(String location, final String query, final
    SearchModelResponseCallback responseCallback) {

        //Retrieve places
        isLoading = true;
        lastSearchQuery = null;
        currentSearchQuery = query;

        Call<SearchResult> searchRequest =
                PlacesRestClient.getInstance().getPlacesInterface().searchPlaces(location, query);

        searchRequest.enqueue(new Callback<SearchResult>() {
            @Override
            public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                lastSearchResult = response.body();
                lastSearchQuery = query;
                currentSearchQuery = null;
                hasErrors = false;
                isLoading = false;
                responseCallback.onResult(lastSearchResult); //notify result
            }

            @Override
            public void onFailure(Call<SearchResult> call, Throwable t) {
                lastSearchResult = null;
                lastSearchQuery = null;
                currentSearchQuery = null;
                hasErrors = true;
                isLoading = false;
                responseCallback.onError(t); //notify error
            }
        });
    }

    @Override
    public boolean hasResultData() {
        return lastSearchResult != null;
    }

    @Override
    public SearchResult getLastSearchResult() {
        return lastSearchResult;
    }

    @Override
    public String getLastSearchQuery() {
        return lastSearchQuery;
    }

    @Override
    public String getCurrentSearchingQuery() {
        return currentSearchQuery;
    }

}
