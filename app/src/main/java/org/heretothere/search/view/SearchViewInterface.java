package org.heretothere.search.view;

import org.heretothere.common.view.CommonViewInterface;

import java.util.ArrayList;

/**
 * (MVP) VIEW Interface for the 'Search' use case.
 */
public interface SearchViewInterface extends CommonViewInterface {

    /**
     * Update the search result list using provided data.
     *
     * @param searchResults a list of {@link SearchResultItem}
     */
    void updateSearchResults(ArrayList<SearchResultItem> searchResults);

    /**
     * Finish the search returning a result
     *
     * @param result a {@link SearchResultItem}, or null if there's no selected result
     */
    void finishWithResult(SearchResultItem result);

}
