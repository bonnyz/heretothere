package org.heretothere.search.view;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import org.heretothere.common.model.api.pojos.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple POJO which can be used to instantiate a {@link SearchResultAdapter}.<br>
 * <b>It represents what the VIEW knows and requires in terms of data.</b>
 */
public class SearchResultItem implements Parcelable {


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * Result image url
     */
    private String imageUrl;

    /**
     * Result title
     */
    private String title;

    /**
     * Result vicinity
     */
    private String vicinity;

    /**
     * Result geo position
     */
    private List<Double> geoPosition;

    public SearchResultItem(Parcel in) {
        readFromParcel(in);
    }


    /* ---------------------------------------------------
     * CONSTRUCTORS
     * ------------------------------------------------- */

    public SearchResultItem(@NonNull Item rawItem) {
        imageUrl = rawItem.getIcon();
        title = rawItem.getTitle();
        vicinity = rawItem.getVicinity();
        geoPosition = rawItem.getPosition();
    }

    public SearchResultItem(String title, String vicinity, String imageUrl, List<Double>
            geoPosition) {
        this.title = title;
        this.vicinity = vicinity;
        this.imageUrl = imageUrl;
        this.geoPosition = geoPosition;
    }


    /* ---------------------------------------------------
     * CLASS METHODS
     * ------------------------------------------------- */

    /**
     * Get the image url
     *
     * @return Image url as a {@link String}
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Get the title/name
     *
     * @return Title as a {@link String}
     */
    public String getTitle() {
        return title;
    }

    /**
     * Get the vicinity
     *
     * @return Vicinity as a {@link String}
     */
    public String getVicinity() {
        return vicinity;
    }

    /**
     * Get the geo-position
     *
     * @return a list containing latitude and longitude
     */
    public List<Double> getGeoPosition() {
        return geoPosition;
    }


    /* ---------------------------------------------------
     * PARCELABLE
     * ------------------------------------------------- */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(imageUrl);
        parcel.writeString(title);
        parcel.writeString(vicinity);
        parcel.writeList(geoPosition);
    }

    public void readFromParcel(Parcel parcel) {
        imageUrl = parcel.readString();
        title = parcel.readString();
        vicinity = parcel.readString();
        geoPosition = new ArrayList<>();
        parcel.readList(geoPosition, Double.class.getClassLoader());
    }

    public static final Creator<SearchResultItem> CREATOR = new Creator<SearchResultItem>() {
        @Override
        public SearchResultItem createFromParcel(Parcel in) {
            return new SearchResultItem(in);
        }

        @Override
        public SearchResultItem[] newArray(int size) {
            return new SearchResultItem[size];
        }
    };
}