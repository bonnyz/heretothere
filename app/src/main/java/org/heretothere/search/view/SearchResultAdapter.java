package org.heretothere.search.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.heretothere.R;
import org.heretothere.utils.HtmlUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * RecyclerView.Adapter used to display {@link SearchResultItem} in a RecyclerView.
 */
class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {


    /* ---------------------------------------------------
     * STATIC CLASSES / INTERFACES
     * ------------------------------------------------- */

    /**
     * RecyclerView.ViewHolder for the {@link SearchResultAdapter}.
     */
    static class ViewHolder extends RecyclerView.ViewHolder {

        /**
         * ViewHolder's root
         */
        View rootView;

        /**
         * Image icon
         */
        @BindView(R.id.row_search_result_icon)
        ImageView imgIcon;

        /**
         * Primary text
         */
        @BindView(R.id.row_search_result_name)
        TextView txtName;

        /**
         * Secondary text
         */
        @BindView(R.id.row_search_result_extra)
        TextView txtExtra;

        ViewHolder(View view) {
            super(view);
            rootView = view;
            ButterKnife.bind(this, view);
        }

        /**
         * Bind view holder's root with the provided click listener
         *
         * @param positionRow
         * @param clickListener
         */
        void bindListener(final SearchResultItem positionRow, final
        SearchResultClickListener clickListener) {

            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onSearchResultClicked(positionRow);
                }
            });
        }
    }

    /**
     * ViewHolder's click callback
     */
    interface SearchResultClickListener {
        void onSearchResultClicked(SearchResultItem itm);
    }


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * SearchResultAdapter data reference
     */
    private ArrayList<SearchResultItem> searchResults;

    /**
     * SearchResultAdapter click listener
     */
    private SearchResultClickListener clickListener;


    /* ---------------------------------------------------
     * CONSTRUCTORS
     * ------------------------------------------------- */

    /**
     * Creates a {@link SearchResultAdapter} using the provided {@link SearchResultItem}s
     *
     * @param searchResults a list of {@link SearchResultItem}
     */
    SearchResultAdapter(ArrayList<SearchResultItem> searchResults, SearchResultClickListener
            clickListener) {
        this.searchResults = searchResults;
        this.clickListener = clickListener;
    }


    /* ---------------------------------------------------
     * CLASS METHODS
     * ------------------------------------------------- */

    @Override
    public SearchResultAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_search_result, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SearchResultItem positionRow = searchResults.get(position);

        //Picasso loads images asynchronously
        if (positionRow.getImageUrl() != null) {
            Picasso.with(holder.imgIcon.getContext())
                    .load(positionRow.getImageUrl())
                    .placeholder(R.drawable.search_placeholder)
                    .into(holder.imgIcon);
        }

        //Update fields
        holder.txtName.setText(positionRow.getTitle());
        holder.txtExtra.setText(HtmlUtils.fromHtml(positionRow.getVicinity()));

        //Rebind click listener
        holder.bindListener(positionRow, clickListener);
    }

    @Override
    public int getItemCount() {
        return searchResults != null ? searchResults.size() : 0;
    }

}
