package org.heretothere.search.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import org.heretothere.R;
import org.heretothere.common.presenter.PresenterStateMaintainerFragment;
import org.heretothere.search.presenter.SearchPresenter;
import org.heretothere.search.presenter.SearchPresenterInterface;

import java.util.ArrayList;

/**
 * (MVP) VIEW implementation of the 'Search' use case.
 */
public class SearchActivity extends AppCompatActivity implements SearchViewInterface {


    /* ---------------------------------------------------
     * STATIC ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * Result code for a successful search
     */
    public static final int RESULT_CODE_SUCCESS = 9;

    /**
     * Bundle key used to fetch/store the result
     */
    public static final String RESULT_KEY = "result";


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * Fragment which handles the search ui
     */
    private SearchFragment searchFragment;

    /**
     * (MVP) PRESENTER reference for the 'Search' use case
     */
    private SearchPresenterInterface searchPresenter;


    /* ---------------------------------------------------
     * LIFE CYCLE
     * ------------------------------------------------- */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.search_title);
        setContentView(R.layout.activity_search);

        FragmentManager fm = getSupportFragmentManager();

        //Obtain a PRESENTER instance
        String retainTag = SearchPresenter.RETAIN_TAG;
        Fragment presenterStateMaintainer = fm.findFragmentByTag(retainTag);
        if (presenterStateMaintainer == null) {
            //there's no cached PRESENTER, create a new one and preserve it in a retainInstance
            // ui-less fragment
            searchPresenter = new SearchPresenter(this);
            FragmentTransaction transaction = fm.beginTransaction();
            PresenterStateMaintainerFragment maintainerFragment = new
                    PresenterStateMaintainerFragment();
            maintainerFragment.setPresenter(searchPresenter);
            transaction.add(maintainerFragment, retainTag);
            transaction.commit();
        } else {
            //retrieve the PRESENTER from the state maintainer fragment
            searchPresenter = (SearchPresenter) ((PresenterStateMaintainerFragment)
                    presenterStateMaintainer).getPresenter();
            //notify the PRESENTER that the VIEW is changed
            searchPresenter.onViewChanged(this);
        }

        //Create/retrieve the SearchFragment
        Fragment cachedSearchFragment = fm.findFragmentByTag(SearchFragment.TAG);
        if (cachedSearchFragment != null && cachedSearchFragment instanceof SearchFragment) {
            searchFragment = (SearchFragment) cachedSearchFragment;
        } else {
            searchFragment = new SearchFragment();
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.activity_search_container, searchFragment, SearchFragment.TAG);
            transaction.commit();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        //notify PRESENTER: view is ready
        searchPresenter.onViewReady();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //notify PRESENTER: view as been destroyed
        searchPresenter.onViewDestroyed();
        searchPresenter = null;
    }


    /* ---------------------------------------------------
     * MVP
     * ------------------------------------------------- */

    @Override
    public void showLoadingIndicator() {
        searchFragment.setLoading(true);
    }

    @Override
    public void hideLoadingIndicator() {
        searchFragment.setLoading(false);
    }

    @Override
    public void updateSearchResults(ArrayList<SearchResultItem> searchResults) {
        searchFragment.setResults(searchResults);
    }

    @Override
    public void finishWithResult(SearchResultItem result) {
        if (result != null) {
            //Set the result before finish
            Intent resultIntent = new Intent();
            resultIntent.putExtra(RESULT_KEY, result);
            setResult(RESULT_CODE_SUCCESS, resultIntent);
        }
        finish();
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.generic_error, Toast.LENGTH_SHORT).show();
    }

    /**
     * Return the 'Search' presenter
     *
     * @return a {@link SearchPresenterInterface}
     */
    public SearchPresenterInterface getPresenter() {
        return searchPresenter;
    }

}
