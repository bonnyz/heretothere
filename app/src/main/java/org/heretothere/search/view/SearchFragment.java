package org.heretothere.search.view;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.heretothere.R;
import org.heretothere.utils.AlphaTouchListener;
import org.heretothere.utils.PositionUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Fragment which handles the 'search' use case.
 */
public class SearchFragment extends Fragment {


    /* ---------------------------------------------------
     * STATIC ATTRIBUTES
     * ------------------------------------------------- */

    public static final String TAG = "SearchFragment";

    /**
     * <i>Search As You Type</i> delay
     */
    private static final int SAYT_DELAY_MS = 500;


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * Search loading indicator
     */
    @BindView(R.id.search_progress_indicator)
    ProgressBar progressSearchLoading;

    /**
     * Search icon
     */
    @BindView(R.id.search_img_icon)
    ImageView imgSearch;

    /**
     * Search input text
     */
    @BindView(R.id.search_etxt_input)
    EditText etxtSearch;

    /**
     * RecyclerView which will show the list of search results
     */
    @BindView(R.id.search_results_recyclerview)
    RecyclerView recyclerViewSearch;

    /**
     * Unbinder ButterKnife
     */
    Unbinder unbinder;

    /**
     * Internal handler for <i>Search As You Type</i>
     */
    Handler saytHandler;


    /* ---------------------------------------------------
     * LIFE CYCLE
     * ------------------------------------------------- */

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
            Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_search, container, false);

        //bind views
        unbinder = ButterKnife.bind(this, root);

        //setup recycler views as a list
        recyclerViewSearch.setHasFixedSize(true);
        recyclerViewSearch.setLayoutManager(new LinearLayoutManager(getActivity()));

        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bindListeners();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        saytHandler.removeCallbacksAndMessages(null);
        saytHandler = null;
    }


    /* ---------------------------------------------------
     * CLASS METHODS
     * ------------------------------------------------- */

    /**
     * Bind views listeners
     */
    private void bindListeners() {
        imgSearch.setOnTouchListener(new AlphaTouchListener(0.2f, imgSearch.getAlpha()));
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notifyPresenterSearch(etxtSearch.getText().toString(), true);
            }
        });

        etxtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    notifyPresenterSearch(etxtSearch.getText().toString(), true);
                    return true;
                }
                return false;
            }
        });

        //"Search As You Type"
        saytHandler = new Handler();
        etxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (saytHandler != null) {
                    final String currentQuery = etxtSearch.getText().toString();
                    saytHandler.removeCallbacksAndMessages(null);
                    saytHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            notifyPresenterSearch(currentQuery, false);
                        }
                    }, SAYT_DELAY_MS);
                }
            }
        });
    }

    /**
     * Internal method that handles search queries
     *
     * @param query        the query to search
     * @param hideKeyboard toggle keyboard visibility
     */
    private void notifyPresenterSearch(String query, boolean hideKeyboard) {
        if (getActivity() != null) {
            ((SearchActivity) getActivity()).getPresenter().onUserSearch(query, PositionUtils
                    .getLastValidPosition());

            if (hideKeyboard) {
                etxtSearch.clearFocus();
                InputMethodManager in = (InputMethodManager) getActivity().getSystemService
                        (Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(etxtSearch.getWindowToken(), 0);
            }
        }
    }

    /**
     * Set the results to be displayed
     *
     * @param searchResults
     */
    public void setResults(ArrayList<SearchResultItem> searchResults) {
        if (recyclerViewSearch != null) {
            recyclerViewSearch.swapAdapter(new SearchResultAdapter(searchResults, new
                    SearchResultAdapter.SearchResultClickListener() {
                        @Override
                        public void onSearchResultClicked(SearchResultItem item) {
                            ((SearchActivity) getActivity()).getPresenter().onUserSelectResult
                                    (item);
                        }
                    }), false);
        }
    }

    /**
     * Set the visibility of the loading indicator
     *
     * @param loading
     */
    public void setLoading(boolean loading) {
        if (progressSearchLoading != null) {
            progressSearchLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        }

        if (recyclerViewSearch != null) {
            recyclerViewSearch.setVisibility(loading ? View.INVISIBLE : View.VISIBLE);
        }
    }

}
