package org.heretothere.search.presenter;

import org.heretothere.common.presenter.CommonPresenterInterface;
import org.heretothere.search.view.SearchResultItem;
import org.heretothere.search.view.SearchViewInterface;

/**
 * (MVP) PRESENTER Interface for the 'Search' use case.
 */
public interface SearchPresenterInterface extends CommonPresenterInterface {

    /**
     * Called when the user search for places
     */
    void onUserSearch(String query, String currentPosition);

    /**
     * Called when the user search for places
     */
    void onUserSelectResult(SearchResultItem item);

    /**
     * Called when the user explicitly press back
     */
    void onUserBackPressed();

    /**
     * Called when the VIEW is changed (e.g. on configuration changes)
     *
     * @param view the new SearchViewInterface
     */
    void onViewChanged(SearchViewInterface view);

}
