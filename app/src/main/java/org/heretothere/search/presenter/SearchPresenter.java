package org.heretothere.search.presenter;

import org.heretothere.common.model.api.pojos.Item;
import org.heretothere.common.model.api.pojos.SearchResult;
import org.heretothere.common.presenter.CommonPresenter;
import org.heretothere.search.model.SearchModel;
import org.heretothere.search.model.SearchModelInterface;
import org.heretothere.search.model.SearchModelResponseCallback;
import org.heretothere.search.view.SearchResultItem;
import org.heretothere.search.view.SearchViewInterface;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * (MVP) PRESENTER implementation of the 'Search' use case.
 */
public class SearchPresenter extends CommonPresenter implements SearchPresenterInterface {


    /* ---------------------------------------------------
     * STATIC ATTRIBUTES
     * ------------------------------------------------- */

    public static final String TAG = "SearchPresenter";

    public static final String RETAIN_TAG = TAG + "_retain";


    /* ---------------------------------------------------
     * ATTRIBUTES
     * ------------------------------------------------- */

    /**
     * Search VIEW reference
     */
    private WeakReference<SearchViewInterface> weakSearchView;

    /**
     * Search MODEL reference
     */
    private SearchModelInterface searchModel;


    /* ---------------------------------------------------
     * CONSTRUCTORS
     * ------------------------------------------------- */

    public SearchPresenter(SearchViewInterface searchView) {
        this.weakSearchView = new WeakReference<SearchViewInterface>(searchView);
        this.searchModel = new SearchModel();
    }

    public SearchPresenter(SearchViewInterface searchView, SearchModelInterface searchModel) {
        this.weakSearchView = new WeakReference<SearchViewInterface>(searchView);
        this.searchModel = searchModel;
    }


    /* ---------------------------------------------------
     * MVP
     * ------------------------------------------------- */

    @Override
    public void onUserSearch(String query, String currentPosition) {
        if (query != null && (query.equals(searchModel.getLastSearchQuery())
                || query.equals(searchModel.getCurrentSearchingQuery())))
            return;

        if (weakSearchView != null && weakSearchView.get() != null) {
            weakSearchView.get().showLoadingIndicator();
        }

        searchModel.getPlacesByQuery(currentPosition, query, new SearchModelResponseCallback() {
            @Override
            public void onResult(SearchResult result) {
                if (weakSearchView != null && weakSearchView.get() != null) {
                    weakSearchView.get().hideLoadingIndicator();
                    weakSearchView.get().updateSearchResults(prepareSearchResultItems(result));
                }
            }

            @Override
            public void onError(Throwable error) {
                if (weakSearchView != null && weakSearchView.get() != null) {
                    weakSearchView.get().hideLoadingIndicator();
                    weakSearchView.get().showError();
                }
            }
        });
    }

    @Override
    public void onUserSelectResult(SearchResultItem item) {
        if (weakSearchView != null && weakSearchView.get() != null) {
            weakSearchView.get().finishWithResult(item);
        }
    }

    @Override
    public void onUserBackPressed() {
        if (weakSearchView != null && weakSearchView.get() != null) {
            weakSearchView.get().finishWithResult(null);
        }
    }

    @Override
    public void onViewChanged(SearchViewInterface view) {
        weakSearchView = new WeakReference<SearchViewInterface>(view);
        restoreOnViewReady = true;
    }

    @Override
    public void onViewReady() {
        if (shouldRestoreDataOnViewReady() && weakSearchView != null && weakSearchView.get() !=
                null) {
            restoreOnViewReady = false;
            SearchViewInterface v = weakSearchView.get();
            if (searchModel.isLoading()) {
                v.showLoadingIndicator();
            } else if (searchModel.hasResultData()) {
                v.updateSearchResults(prepareSearchResultItems(searchModel.getLastSearchResult()));
            }
        }
    }

    @Override
    public void onViewDestroyed() {
        weakSearchView = null;
    }

    /**
     * Prepare data for the SearchViewInterface.
     *
     * @param rawResult a raw {@link SearchResult}
     * @return a list of {@link SearchResultItem}
     */
    public ArrayList<SearchResultItem> prepareSearchResultItems(SearchResult rawResult) {
        ArrayList<SearchResultItem> preparedResult = new ArrayList<>();
        if (rawResult != null && rawResult.getResults() != null && rawResult.getResults()
                .getItems() != null) {
            for (Item rawItem : rawResult.getResults().getItems()) {
                preparedResult.add(new SearchResultItem(rawItem));
            }
        }
        return preparedResult;
    }

    /**
     * Return the reference to the {@link SearchViewInterface}
     *
     * @return a {@link SearchViewInterface}
     */
    public SearchViewInterface getSearchViewInterface() {
        if (weakSearchView != null) {
            return weakSearchView.get();
        }
        return null;
    }

}
