package org.heretothere;

import org.heretothere.search.model.SearchModelInterface;
import org.heretothere.search.model.SearchModelResponseCallback;
import org.heretothere.search.presenter.SearchPresenter;
import org.heretothere.search.view.SearchResultItem;
import org.heretothere.search.view.SearchViewInterface;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.heretothere.TestUtils.getIconForIndex;
import static org.heretothere.TestUtils.getSampleRawResult;
import static org.heretothere.TestUtils.getSampleSearchResultItems;
import static org.heretothere.TestUtils.getTitleForIndex;
import static org.heretothere.TestUtils.getVicinityForIndex;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class SearchPresenterTest {

    /** Mocked SearchView */
    @Mock
    SearchViewInterface searchViewInterface;

    /** Mocked SearchModel */
    @Mock
    SearchModelInterface searchModelInterface;

    /** Callback captor, used to simulate different search responses */
    @Captor
    ArgumentCaptor<SearchModelResponseCallback> mockSearchResponseCallback;

    /** SearchPresenter to test */
    private SearchPresenter presenter;

    @Before
    public void initPresenter() {
        //init presenter using mocked VIEW and MODEL
        presenter = new SearchPresenter(searchViewInterface, searchModelInterface);
    }

    /**
     * Test case: init
     */
    @Test
    public void checkInit() {
        assertNotNull(presenter);
        assertNotNull(presenter.getSearchViewInterface());
    }

    /**
     * Test case: exit without result
     */
    @Test
    public void checkExitWithoutResult() {
        presenter.onUserBackPressed();
        verify(searchViewInterface, times(1)).finishWithResult(null);
    }

    /**
     * Test case: search without results
     */
    @Test
    public void checkSearchWithNoResult() {
        partialCheckOnUserSearch();
        mockSearchResponseCallback.getValue().onResult(null);
        verify(searchViewInterface, times(1)).hideLoadingIndicator();
        verify(searchViewInterface, times(1)).updateSearchResults(eq(new
                ArrayList<SearchResultItem>()));
    }

    /**
     * Test case: search with results
     */
    @Test
    public void checkSearchWithResult() {
        partialCheckOnUserSearch();
        mockSearchResponseCallback.getValue().onResult(getSampleRawResult());
        verify(searchViewInterface, times(1)).hideLoadingIndicator();
        verify(searchViewInterface, times(1))
                .updateSearchResults(refEq(getSampleSearchResultItems()));
    }

    /**
     * Test case: search with results and result selection
     */
    @Test
    public void checkSearchWithResultAndSelection() {
        checkSearchWithResult();
        SearchResultItem resultItem = getSampleSearchResultItems().get(0);
        presenter.onUserSelectResult(resultItem);
        verify(searchViewInterface, times(1)).finishWithResult(resultItem);
    }

    /**
     * Test case: search with error
     */
    @Test
    public void checkSearchWithError() {
        partialCheckOnUserSearch();
        mockSearchResponseCallback.getValue().onError(new Throwable());
        verify(searchViewInterface, times(1)).hideLoadingIndicator();
        verify(searchViewInterface, times(1)).showError();
    }

    /**
     * Test case: prepare raw data for the view
     */
    @Test
    public void checkPrepareSearchResult() {
        ArrayList<SearchResultItem> searchResultItems = presenter.prepareSearchResultItems
                (getSampleRawResult());
        assertNotNull(searchResultItems);
        assertTrue(searchResultItems.size() > 0);
        for (int i = 0; i < searchResultItems.size(); i++) {
            assertTrue(searchResultItems.get(i).getTitle().equals(getTitleForIndex(i)));
            assertTrue(searchResultItems.get(i).getImageUrl().equals(getIconForIndex(i)));
            assertTrue(searchResultItems.get(i).getVicinity().equals(getVicinityForIndex(i)));
        }
    }

    /**
     * Partial check: start a search
     */
    private void partialCheckOnUserSearch() {
        String query = "test_" + System.currentTimeMillis();
        String location = "50,49";
        presenter.onUserSearch(query, location);
        verify(searchViewInterface, times(1)).showLoadingIndicator();
        verify(searchModelInterface, times(1))
                .getPlacesByQuery(eq(location), eq(query), mockSearchResponseCallback.capture());
    }



}
