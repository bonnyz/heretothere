package org.heretothere;

import com.here.android.mpa.common.GeoPolyline;

import org.heretothere.common.model.api.pojos.CalculatedRoute;
import org.heretothere.routing.model.RouteModelInterface;
import org.heretothere.routing.model.RouteModelResponseCallback;
import org.heretothere.routing.presenter.RoutePresenter;
import org.heretothere.routing.view.RouteViewInterface;
import org.heretothere.search.view.SearchResultItem;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import static junit.framework.Assert.assertNotNull;
import static org.heretothere.TestUtils.getSampleSearchResultItems;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class RoutePresenterTest {

    /** Mocked RouteView */
    @Mock
    RouteViewInterface routeViewInterface;

    /** Mocked RouteModel */
    @Mock
    RouteModelInterface routeModelInterface;

    /** Callback captor, used to simulate different search responses */
    @Captor
    ArgumentCaptor<RouteModelResponseCallback> mockRouteResponseCallback;

    /** RoutePresenter to test */
    private RoutePresenter presenter;

    @Before
    public void initPresenter() {
        //init presenter using mocked VIEW and MODEL
        presenter = new RoutePresenter(routeViewInterface, routeModelInterface);
    }

    /**
     * Test case: init
     */
    @Test
    public void checkInit() {
        assertNotNull(presenter);
        assertNotNull(presenter.getRouteViewInterface());
    }

    /**
     * Test case: open search for places
     */
    @Test
    public void checkOpenSearch(){
        presenter.onUserSearchPlace(true);
        verify(routeViewInterface, times(1)).openSearch(true);
        reset(routeViewInterface);
        presenter.onUserSearchPlace(false);
        verify(routeViewInterface, times(1)).openSearch(false);
    }

    /**
     * Test case: select points
     */
    @Test
    public void checkPlaceSelection(){
        partialPlaceSelection(false);
    }

    /**
     * Test case: calculate error when using same points
     */
    @Test
    public void checkCalculate(){
        partialPlaceSelection(false);
        presenter.onUserCalculateRoute();
        verify(routeViewInterface, times(1)).showLoadingIndicator();
        verify(routeModelInterface, times(1)).getRoute(any(String.class), any(String.class),
                any(String.class), any(String.class), mockRouteResponseCallback.capture());
        mockRouteResponseCallback.getValue().onResult(new CalculatedRoute());
        verify(routeViewInterface, times(1)).updateRoute(any(GeoPolyline.class));
        verify(routeViewInterface, times(1)).hideLoadingIndicator();
    }

    /**
     * Test case: calculate route error if using same points
     */
    @Test
    public void checkErrorSamePlaceCalculate(){
        partialPlaceSelection(true);
        presenter.onUserCalculateRoute();
        verify(routeViewInterface, times(1)).showSamePointsError();
    }

    /**
     * Partial check for the start/destination selection
     * @param useSamePlaces
     */
    private void partialPlaceSelection(boolean useSamePlaces){
        ArrayList<SearchResultItem> sampleSearchResultItems = getSampleSearchResultItems();
        SearchResultItem startingPoint = sampleSearchResultItems.get(0);
        presenter.onUserPlaceSelected(true, startingPoint);
        SearchResultItem destinationPoint = sampleSearchResultItems.get(1);
        verify(routeViewInterface, times(1)).updateStartPoint(startingPoint);
        presenter.onUserPlaceSelected(false, useSamePlaces ? startingPoint : destinationPoint);
        verify(routeViewInterface, times(1)).updateEndPoint(useSamePlaces ? startingPoint : destinationPoint);
        verify(routeViewInterface, times(1)).enableCalculateRoute();
    }

}
