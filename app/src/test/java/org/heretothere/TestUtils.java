package org.heretothere;

import org.heretothere.common.model.api.pojos.Item;
import org.heretothere.common.model.api.pojos.Results;
import org.heretothere.common.model.api.pojos.Search;
import org.heretothere.common.model.api.pojos.SearchResult;
import org.heretothere.search.view.SearchResultItem;

import java.util.ArrayList;

/**
 * Utility class for testing
 */
public class TestUtils {

    /**
     * Generate a predefined {@link SearchResult}
     */
    public static SearchResult getSampleRawResult() {
        Results r = new Results(getSampleRawItems());
        Search s = new Search(null);
        SearchResult sr = new SearchResult(r, s);
        return sr;
    }

    /**
     * Generate a predefined {@link Item} list
     */
    public static ArrayList<Item> getSampleRawItems() {
        ArrayList<Item> items = new ArrayList<>();
        for (int i = 0; i < 4; i++) {

            ArrayList<Double> pos = new ArrayList<>();
            pos.add((double) (i+1));
            pos.add((double) (i+2));

            items.add(new Item(pos, null, 0, getTitleForIndex(i), null, getIconForIndex(i),
                    getVicinityForIndex(i), null, null, null, null, false));
        }
        return items;
    }

    /**
     * Generate a predefined {@link SearchResultItem} list
     */
    public static ArrayList<SearchResultItem> getSampleSearchResultItems() {
        ArrayList<Item> sampleRawItems = getSampleRawItems();
        ArrayList<SearchResultItem> prepared = new ArrayList<SearchResultItem>();
        for (Item item : sampleRawItems) {
            prepared.add(new SearchResultItem(item));
        }
        return prepared;
    }

    /**
     * Generate a predefined title for the given index
     *
     * @param index
     * @return
     */
    public static String getTitleForIndex(int index) {
        return "index" + index;
    }

    /**
     * Generate a predefined icon for the given index
     *
     * @param index
     * @return
     */
    public static String getIconForIndex(int index) {
        return "index" + index;
    }

    /**
     * Generate a predefined vicinity for the given index
     *
     * @param index
     * @return
     */
    public static String getVicinityForIndex(int index) {
        return "index" + index;
    }
}
